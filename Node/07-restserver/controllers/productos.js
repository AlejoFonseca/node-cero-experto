const { request, response } = require("express");
const { Categoria, Producto } = require('../models');


const obtenerProductos = async ( req = request, res = response ) => {

    const { limite = 5, desde = 0 } = req.query;
    const query = { estado: true };

    const [total, productos] = await Promise.all([
        Producto.countDocuments(query),
        Producto.find(query)
                .skip(Number(desde))
                .limit(Number(limite))
                .populate('usuario', 'nombre')
                .populate('categoria', 'nombre')
    ]);

    res.json({
        total,
        productos
    });
}

const obtenerProducto = async ( req = request, res = response ) => {

    const id = req.params.id;
    const producto = await Producto.findById( id )
                                     .populate('usuario', 'nombre')
                                     .populate('categoria', 'nombre');

    return res.status(200).json(producto);
}

const crearProducto = async ( req = request, res = response ) => {

    const nombre = req.body.nombre.toUpperCase();

    const productoDB = await Producto.findOne( {nombre} );

    if ( productoDB ) {
        return res.status(400).json({
            msg: `El producto ${ productoDB.nombre }, ya existe`
        });
    }

    // Generar la data a guardar
    const data = {
        nombre,
        usuario: req.usuario._id,
        categoria: req.body.categoria
    }

    const producto = new Producto( data );

    // Guardar DB
    await producto.save();

    res.status(201).json( producto );
}

const actualizarProducto = async ( req = request, res = response ) => {

    const id = req.params.id;
    const { estado, usuario, ...data} = req.body;

    if (data.categoria) {
        const categoria = await Categoria.findById( data.categoria );
        if (!categoria) { return res.json({msg: 'La categoria no es válida'}) }
    }

    data.nombre = data.nombre.toUpperCase();
    data.usuario = req.usuario._id;

    const producto = await Producto.findByIdAndUpdate( id, data, {new: true} );

    res.json( producto );
}

const borrarProducto = async ( req = request, res = response ) => {

    const id = req.params.id;
    const prductoBorrado = await Producto.findByIdAndUpdate( id, {estado: false}, {new: true} );

    res.json( prductoBorrado );
}

module.exports = {
    obtenerProductos,
    obtenerProducto,
    crearProducto,
    actualizarProducto,
    borrarProducto
}