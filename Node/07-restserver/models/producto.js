const { Schema, model } = require('mongoose');

const ProductoSchema = Schema({
    nombre: {
        type: String,
        required: [true, 'El nombre es obligatorio'],
        unique: true
    },
    estado: {
        type: Boolean,
        default: true,
        required: true
    },
    usuario: {
        type: Schema.Types.ObjectId,
        ref: 'Usuario',
        required: true
    },
    precio: {
        type: Number,
        default: 0
    },
    categoria: {
        type: Schema.Types.ObjectId,
        ref: 'Categoria',
        required: true
    },
    descripcion: { type: String },
    disponible: { type: Boolean, default: true },
    img: { type: String }
});

// Cada vez que se emita la funcion, excluirá __v y el estado del objeto
ProductoSchema.methods.toJSON = function() {
    //this.toObject es un metodo de moongose para convertir un Schema en un Objeto JS y poder controlarlo como objeto
    const { __v, estado, ...categoria } = this.toObject();
    return categoria;
}

module.exports = model('Producto', ProductoSchema);