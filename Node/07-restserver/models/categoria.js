const { Schema, model } = require('mongoose');

const CategoriaSchema = Schema({
    nombre: {
        type: String,
        required: [true, 'El nombre es obligatorio'],
        unique: true
    },
    estado: {
        type: Boolean,
        default: true,
        required: true
    },
    usuario: {
        type: Schema.Types.ObjectId,
        ref: 'Usuario',
        required: true
    }
});

// Cada vez que se emita la funcion, excluirá __v y el estado del objeto
CategoriaSchema.methods.toJSON = function() {
    //this.toObject es un metodo de moongose para convertir un Schema en un Objeto JS y poder controlarlo como objeto
    const { __v, estado, ...categoria } = this.toObject();
    return categoria;
}

module.exports = model('Categoria', CategoriaSchema);