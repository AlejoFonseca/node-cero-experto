const { Router } = require('express');
const { check } = require('express-validator');
const { crearCategoria, obtenerCategoria, obtenerCategorias, actualizarCategoria, borrarCategoria } = require('../controllers/categorias');
const { existeCategoriaPorId } = require('../helpers/db-validators');

const { validarCampos,
        validarJWT, 
        esAdminRole,
        tieneRole } = require('../middlewares');

const router = Router();

// Obtener todas las categorías
router.get('/', obtenerCategorias);

// Obtener una categoría por id
router.get('/:id', [
    check('id', 'No es un id de mongo válido').isMongoId(),
    check('id').custom( existeCategoriaPorId ),
    validarCampos 
], obtenerCategoria);

// Crear una categoría
router.post('/',[
    validarJWT,
    check('nombre', 'El nombre es obligatorio').notEmpty(),
    validarCampos
], crearCategoria );

// Actualizar una categoría
router.put('/:id', [
    validarJWT,
    check('nombre', 'El nombre es obligatorio').notEmpty(),
    check('id').custom( existeCategoriaPorId ),
    validarCampos ],
    actualizarCategoria);

// Borrar una categoría
router.delete('/:id', [
    validarJWT,
    esAdminRole,
    check('id', 'No es un id de mongo válido').isMongoId(),
    check('id').custom( existeCategoriaPorId ),
    validarCampos ],
    borrarCategoria);

module.exports = router;