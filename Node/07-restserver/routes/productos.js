const { Router } = require('express');
const { check } = require('express-validator');
const { crearProducto, obtenerProductos, obtenerProducto, actualizarProducto, borrarProducto } = require('../controllers/productos');
const { existeProductoPorId, existeCategoriaPorId } = require('../helpers/db-validators');

const { validarCampos,
        validarJWT, 
        esAdminRole,
        tieneRole } = require('../middlewares');

const router = Router();

// Obtener todos los productos
router.get('/', obtenerProductos);

// Obtener un producto por id
router.get('/:id', [
    check('id', 'No es un id de mongo válido').isMongoId(),
    check('id').custom( existeProductoPorId ),
    validarCampos 
], obtenerProducto);

// Crear un producto
router.post('/',[
    validarJWT,
    check('nombre', 'El nombre es obligatorio').notEmpty(),
    check('categoria', 'No es un id de mongo válido').isMongoId(),
    check('categoria').custom( existeCategoriaPorId ),
    validarCampos
], crearProducto );

// Actualizar un producto
router.put('/:id', [
    validarJWT,
    check('id', 'No es un id de mongo válido').isMongoId(),
    check('id').custom( existeProductoPorId ),
    validarCampos ],
    actualizarProducto);

// Borrar un producto
router.delete('/:id', [
    validarJWT,
    esAdminRole,
    check('id', 'No es un id de mongo válido').isMongoId(),
    check('id').custom( existeProductoPorId ),
    validarCampos ],
    borrarProducto);

module.exports = router;