const Tarea = require("./tarea");
require('colors');

class Tareas {

    _listado = {};

    get listadoArr() {
         const listado = [];
         Object.keys(this._listado).forEach( key => {

             const tarea = this._listado[key];
             listado.push(tarea);
         });

         return listado;
    }

    constructor() {
        this._listado = {};
    }

    borrarTarea( id = ''){
        if (this._listado[id]) {
            delete this._listado[id];
        }
    }

    crearTarea( desc = '' ) {
        const tarea = new Tarea( desc );
        this._listado[tarea.id] = tarea;
    }

    cargarTareasFromArray( tareas = [] ) {

        tareas.forEach( tarea => {

            this._listado[tarea.id] = tarea;
        });
    }

    listadoCompleto() {

        console.log();

        this.listadoArr.forEach( (tarea, i) => {

            const idx = `${i + 1}` .green;

            const { desc, completado} = tarea;
            const estado = (completado)
                                ? 'Compleatada' .green
                                : 'Pendiente' .red

            console.log(`${idx} ${desc} :: ${estado}`);
        });
    }

    listarPendientesCompletadas( completadas = true) {

        let idx = 0;

        this.listadoArr.forEach( tarea => {
            
            if (completadas) {
                if (tarea.completado) {
                    idx ++;
                    console.log(`${(idx.toString() + '.').green} ${tarea.desc} :: ${tarea.completado.toString() .green} `);
                }
            }else{
                if (!tarea.completado) {
                    idx ++;
                    console.log(`${(idx.toString() + '.').green} ${tarea.desc} :: Pendiente `);
                }
            }
        }); 
    }

    toggleCompletadas( ids = [] ) {

        ids.forEach( id => {
            const tarea = this._listado[id];
            if ( !tarea.completado ) {
                tarea.completado = new Date().toISOString();
            }
        });

        this.listadoArr.forEach( tarea => {
            if ( !ids.includes(tarea.id) ) {
                this._listado[tarea.id].completado = null;
            }
        });
    }
}

module.exports = Tareas;