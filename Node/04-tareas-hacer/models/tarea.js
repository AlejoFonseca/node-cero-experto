const { v4: uuidv4 } = require('uuid');

class Tarea {
    id = '';
    desc = '';
    completado = null;

    constructor( desc ) {
        this.id = uuidv4(); // ⇨ '1b9d6bcd-bbfd-4b2d-9b5d-ab8dfbbd4bed'
        this.desc = desc;
        this.completado = null;
    }
}

module.exports = Tarea;