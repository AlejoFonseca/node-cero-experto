require('colors');
const { guardarDB, leerDB } = require('./helpers/guardarArchivo');
const { inquirerMenu, pausa, leerInput, listadoTareasBorrar, confirmar, mostrarListadoChecklist } = require('./helpers/inquirer');
const Tareas = require('./models/tareas');

const main = async() => {

    console.log(`Hola mundo`);

    let opt = '';
    const tareas = new Tareas();

    const tareasDB = leerDB();

    if (tareasDB) {
        tareas.cargarTareasFromArray(tareasDB);
    }

    do {
        // Imprimir el menu
        opt = await inquirerMenu();

        switch (opt) {
            case '1':
                // crearTarea
                const desc = await leerInput('Descripcion: ');
                tareas.crearTarea( desc );
                break;

            case '2':
                // ListarTarea
                tareas.listadoCompleto();
                break;  
            
            case '3':
                // Listar tareas completadas
                tareas.listarPendientesCompletadas(true);
                break;

            case '4':
                // Listar tareas pendientes
                tareas.listarPendientesCompletadas(false);
                break;

            case '5':
                // Completar tarea(s)
                const completadas = await mostrarListadoChecklist( tareas.listadoArr );

                tareas.toggleCompletadas( completadas );
                break;
            
            case '6':
                // Borrar
                const id = await listadoTareasBorrar( tareas.listadoArr );

                if (id !== '0') {
                    const ok = await confirmar('Está seguro de que desea borrar?');
                    if (ok) { tareas.borrarTarea(id); }
                }
                break;
        }

        guardarDB( tareas.listadoArr );

        await pausa();
        
    } while ( opt !== '0');
    
    
    // pausa();
}

main();