const { inquirerMenu, pausa, leerInput, listarLugares } = require('./helpers/inquirer');
const Busquedas = require('./models/busquedas');

const main = async() => {

    const busquedas = new Busquedas();
    let opt;

    do {

        opt = await inquirerMenu();

        switch (opt) {

            case 1:
                // Mostrar el mensaje
                const busqueda = await leerInput();

                // Buscar los lugares
                const lugares = await busquedas.ciudad( busqueda );
                
                // Seleccionar el lugar
                const id = await listarLugares( lugares );
                if ( id === '0') continue;

                const { nombre, lgn, lat } = lugares.find( l => l.id === id );

                // Guardar en DB
                busquedas.agregarHistorial( nombre );

                // Clima
                const { desc, min, max, temp } = await busquedas.climaLugar( lat, lgn );

                // Mostrar resultados
                console.clear();
                console.log('\nInformación de la ciudad\n'.green);
                console.log('Ciudad:', nombre.green);
                console.log('Lat:', lat);
                console.log('Lgn:', lgn);
                console.log('Temperatura:', temp);
                console.log('Mínima:', min);
                console.log('Máxima:', max);
                console.log('Cómo está el clima:', desc);

                break;

            case 2:

                busquedas.historialCapitalizado.forEach( (lugar, i) => {
                    const idx = `${ i + 1 }.` .green;
                    console.log( `${ idx } ${ lugar }` );
                });

                break;
        }
        

        if (opt !== 0) await pausa();        

    } while (opt !== 0);


}

main();
