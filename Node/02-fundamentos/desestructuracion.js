const deadpool = {
    nombre: 'Wade',
    apellido: 'Winston',
    poder: 'Regeneración',
    getNombre() {
        return `${ this.nombre } ${ this.apellido} ${ this.poder }`
    }
}

// Desestructurar el objeto

// En vez de hacer esto

const nombre = deadpool.nombre;
const apellido = deadpool.apellido;
const poder = deadpool.poder;

// Hacer esto
const { nombre, apellido, poder } = deadpool;

console.log(nombre, apellido, poder);
console.log(deadpool.getNombre());

// Desestructurar array
const heroes = ['Deadpool', 'Superman', 'Batman'];

// En vez de hacer esto
const h1 = heroes[1];
const h2 = heroes[2];
const h3 = heroes[3];

// Hacer esto
const [h1, h2, h3] = heroes;

console.log(h1, h2, h3);